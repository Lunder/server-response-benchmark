import sys

from response_benchmark.benchmark import compare_urls

if __name__ == '__main__':
    compare_urls(*sys.argv[1:])
