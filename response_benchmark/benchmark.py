import requests

from response_benchmark.logger import logger
from response_benchmark.mailing import send_mail

message_email_address = 'example@email.com'


def compare_urls(base_url, *urls):
    requests_list = [requests.get(url) for url in [base_url, *urls]]
    time_of_load_to_compare = requests_list[0].elapsed
    list_of_requests = sorted([{
        'url': r.url,
        'loading_time': str(r.elapsed),
        'response_status': r.status_code,
        'diff_by_url_to_compare': str(r.elapsed - time_of_load_to_compare),
    } for r in requests_list],
        key=lambda request: request['loading_time'])
    if list_of_requests[0]['url'] != base_url:
        send_mail(
            "noreply@localhost",
            message_email_address,
            "There's a faster connection than your!",
            f"Hey, check your script because there's a connection faster than your. Fix it."
        )
    logger.info(compose_result_string(list_of_requests))
    return list_of_requests


def compose_result_string(results_list):
    return "".join(
        [
            f"url: {result['url']: <{80}} response: {result['response_status']: <{15}} time: {result['loading_time']: <{25}} compare: {result['diff_by_url_to_compare']: <{14}}\n"
            for result in results_list]
    )
