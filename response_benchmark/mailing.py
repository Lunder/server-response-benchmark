import smtplib
from email.message import EmailMessage

mail_host = 'localhost'
mail_port = 1025


def send_mail(mail_from, mail_to, mail_subject, mail_content):
    mail_engine = smtplib.SMTP(mail_host, mail_port)
    msg = EmailMessage()

    msg['From'] = mail_from
    msg['To'] = mail_to
    msg['Subject'] = mail_subject
    msg.set_content(mail_content)

    mail_engine.send_message(msg)
    mail_engine.quit()
