import logging

logging.basicConfig(
    level=logging.INFO,
    format="Test finished at %(asctime)s\n%(message)s",
    handlers=[
        logging.FileHandler('benchmark.log'),
        logging.StreamHandler(),
    ]
)
logger = logging.getLogger()