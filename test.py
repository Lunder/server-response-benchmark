from unittest import TestCase, mock
from response_benchmark.benchmark import compare_urls
from datetime import timedelta


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, url, elapsed, status_code):
            self.url = url
            self.elapsed = elapsed
            self.status_code = status_code

    if args[0] == 'http://testurl1.com':
        return MockResponse(
            url=args[0],
            elapsed=timedelta(seconds=4),
            status_code=200
        )
    if args[0] == 'http://testurl2.com':
        return MockResponse(
            url=args[0],
            elapsed=timedelta(seconds=8),
            status_code=200
       )

