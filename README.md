## About

Here is a script that, after entering the list of URLs, sends a requests to them and counts the response time from the servers by sorting them from the fastest answer.

---

## Prepare environment to run script

Warning! Script works with Python 3.6 or newer!

First you need to clone a repository on your system. To do this, go to the folder where you want to download this code, and use command
```bash
git clone https://Lunder@bitbucket.org/Lunder/server-response-benchmark.git
```
Next you need to install all requirements. Go to the folder with project and install necessary packages by pip:
```bash
pip install -r requirements.txt
```
or you can use virtual env to install required packages.

## How to run it?

To run the script and get a report, go to the folder with the code and run the script in main.py file using Python. Pass the URLs separated by a space after the command.

```bash
python main.py https://url1.com https://url2.com https://url3.com
```

You have to pass at least 2 URLs.

---

## Run tests

To run tests, go to the folder with project and run command
```bash
python -m unittest test.py
```

## Created by Łukasz Stępnicki, 2018